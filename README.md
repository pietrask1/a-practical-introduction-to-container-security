# A Practical Introduction to Container Security - a hands on lab.

### Red Hat Summit - April 28-29 2020

#### Deploying the lab guide.

##### As a container

```
$ podman run -d --restart=always --pull=always --name=lab-guide -p 10080:10080 -e WORKSHOP_VARS="{\"student_ssh_password\":\"mypassword\",\"guid\":\"summit\",\"student_ssh_command\":\"ssh lab-user@bastion.summit.com\"}" quay.io/bkozdemb/labguide
```

[bookbag test](bookbag-5mwkp-rhs-2020-t0a529-dev.apps.cluster-rhsummit-dev.rhsummit-dev.events.opentlc.com)
