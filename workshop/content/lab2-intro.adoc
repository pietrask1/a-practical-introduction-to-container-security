:imagesdir: images
:GUID: %guid%
:STUDENT_SSH_COMMAND: %student_ssh_command%
:STUDENT_SSH_PASSWORD: %student_ssh_password%
:USERNAME: %username%
:markup-in-source: verbatim,attributes,quotes
:toc:

This lab session is a low-level, hands-on introduction to container security using the container tools included with Red Hat Enterprise Linux 8. It is intended to be consumed as a series of self paced exercises.

.Prerequisites
* An introductory knowledge of Linux and containers is helpful.
* Basic text editing skills using `vim` or `nano`.

BlueJeans Conferencing Software

.How to get help
image::bluejeans.png[Lab diagram]

Lab Environment 

.Lab Environment
image::lab-diagram.png[Lab diagram]

.Lab Resources
[options="header,footer"]
|=======================
|Server   |Function
|BookBag  |Client as a Service 
|bastion  |External ssh and container host     
|node1    |Image registry and  container host
|node2    |Image registry and  container host
|=======================

==== Important 

TIP: These servers can not be rebuilt in a timely fashion.

.TIPS
* Do not `rm` any files or directories you did not create.
* Make a backup copy before modifying any file.  

==== Conventions used in this lab 

Shells and Prompts

.BookBag
```
%BOOKBAG_PROMPT%
```

.Bastion
```
%BASTION_PROMPT%
```

.Example `*command*` with `sample output`.
[source,subs="{markup-in-source}"]
----
$ *cat /etc/redhat-release*

Red Hat Enterprise Linux release 8.1 (Ootpa)
----

==== The BookBag Environment 

BookBag provides a integrated student client environment in a web browser. It includes the lab guide, terminals and a collection of client programs (i.e. `ssh`) to access the servers used in the lab. In case you are interested, BookBag is hosted in a container running on an OpenShift cluster.

.BookBag Tips
- Increase the width of the web browser to view the table of contents.
- Move the horizontal and vertical dividers as needed.
- Use the menu in the upper right to open additional terminals.
- Commands may be copy and pasted from the lab guide to the terminals.

==== Getting access to the servers

.Bastion
  * `{STUDENT_SSH_COMMAND}`
  * Password: `{STUDENT_SSH_PASSWORD}`

.Nodes
  * To login to the either node from the bastion, `ssh` must be run using `sudo`.

[source,subs="{markup-in-source}"]
```
$ *sudo ssh node1.{GUID}.internal*

[ec2-user@node1 ~]$ 
```

==== Global User ID

Each student is assigned a unique Global User ID (GUID) environment variable that is used throughout the lab.

Your GUID = {GUID}

.Confirm your GUID 
[source,subs="{markup-in-source}"]
```
$ *echo $GUID*

{GUID}
```
==== Optional BookBag Configuration 

This configuration is optional but helpful. Be certain this is
performed in the BookBag shell only.

.Create a an ssh key pair. Accept the default values with no pass phrase.
[source,subs="{markup-in-source}"]
```
%BOOKBAG_PROMPT% *ssh-keygen*
```
.Create an ssh configuration to prevent the ssh session from timing out.
[source,subs="{markup-in-source}"]
```
%BOOKBAG_PROMPT% *echo -e "Host * \nServerAliveInterval 120" >> $HOME/.ssh/config*
```
.Copy the ssh identity to the bastion. Use the bastion hostname and password provided above.
[source,subs="{markup-in-source}"]
```
%BOOKBAG_PROMPT% *ssh-copy-id lab-user@<bastion-hostname>*
```